/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package reto5;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author ISA
 */
public class conexion {
//    String conexionBD="";
//    Connection conex=null;
//    private Statement estado=null;
//    private ResultSet resul=null;
//            
//    
//    public conexion(){
//        conexionBD="Producto.db";
//        
//        try {
//            conex=DriverManager.getConnection(conexionBD);
//            
//            if(conex!=null){
//                DatabaseMetaData meta = conex.getMetaData();
//                System.out.println("Conexión Establecida");
//            }
//        } catch (SQLException e) {
//            System.out.println("Conexion no Establecida");
//        }
//    }
//    
//    public void terminarConexion(Connection conex){
//        if (conex!=null){
//            try {
//                conex.close();
//                System.out.println("Conexión Finalida");
//            } catch (SQLException e) {
//                Logger.getLogger(conexion.class.getName()).log(Level.SEVERE,null,e);
//            }
//        }
//    }
//    
//    public void cerrarCanal(){
//        terminarConexion(conex);
//    } 
//    public boolean insertarBD(String sentenciaSQL){
//        try {
//            estado=conex.createStatement();
//            estado.execute(sentenciaSQL);
//            return true;
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null,"Error en el registro de Datos",null, 0);
//            return false;
//        }
//    }
//
//    boolean setAutoCommitBD(boolean b) {
//        try {
//            conex.setAutoCommit(b);
//            return true;
//        } catch (Exception e) {
//            System.out.println("Error ");
//            return false;
//        }
//    }
//
//    public boolean commitBD() {
//        try {
//            conex.commit();
//            return true;
//        } catch (Exception e) {
//             System.out.println("Error ");
//            return false;
//        }
//         }
//
//    public boolean rollBack() {
//        try {
//            conex.rollback();
//            return true;
//        } catch (Exception e) {
//             System.out.println("Error ");
//            return false;
//        }}
//    
    private String url;
    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;
    
    public conexion(){
        url = "Producto.db";
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:"+url);
            if(con != null){
                System.out.println("Conexion exitosa");
            }
        } catch (Exception e) {
            System.out.println("conoxion erronea");
        }
    }
    
    public List<Producto> listarProducto(){
        List<Producto> datos = new ArrayList<>();
        try {
            stmt = con.prepareStatement("SELECT * FROM productos;");
            rs = stmt.executeQuery();
            while(rs.next()){
                Producto p = new Producto(rs.getString("id"),rs.getString("nombre"),rs.getDouble("temperatura"),rs.getDouble("valorBase"));
                datos.add(p);
            }
        } catch (Exception e) {
            System.out.println("No se han descargado los datos.");
            return null;
        }
        return datos;
    }
    
    public boolean guardarProducto(Producto p){
        try {
            stmt = con.prepareStatement("INSERT INTO tablap(id,nombre,temperatura,valorBase) VALUES (?,?,?,?);");
            stmt.setString(1, p.getId());
            stmt.setString(2, p.getNombre());
            stmt.setDouble(3, p.getTemperatura());
            stmt.setDouble(4, p.getValorBase());
            stmt.executeUpdate();
        } catch (Exception e) {
            System.out.println("No se ha podido guardar");
            return false;
        }
        return true;
    }
    
    public boolean eliminarProducto(String id){
        try {
            stmt = con.prepareStatement("DELETE FROM tablap WHERE id = ?;");
            stmt.setString(1, id);
            stmt.executeUpdate();
        } catch (Exception e) {
            System.out.println("No se elimino");
            return false;
        }
        return true;
    }
    
    public boolean actualizarProducto(Producto p){
        try {
            stmt = con.prepareStatement("UPDATE tablap SET nombre = ?, temperatura = ?, valorBase = ? WHERE id = ?;");
            stmt.setString(1, p.getNombre());
            stmt.setDouble(2, p.getTemperatura());
            stmt.setDouble(3, p.getValorBase());
            stmt.setString(4, p.getId());
            stmt.executeUpdate();
        } catch (Exception e) {
            System.out.println("No se pudo actulziar el producto.");
            return false;
        }
        return true;
    }
    
    public Producto consultarProducto(String id){
        Producto producto;
        try { 
            stmt = con.prepareStatement("SELECT * FROM productos WHERE id = ?;");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            producto = new Producto(rs.getString("id"),rs.getString("nombre"),rs.getDouble("temperatura"),rs.getDouble("valorBase"));
        } catch (Exception e) {
            System.out.println("No se ha encontrado el producto.");
            return null;
        }
        return producto;
    }
    
}
